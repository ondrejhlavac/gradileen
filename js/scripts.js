
// MAIN init function
function masterHandler() {
	valuateDate();
}

// count of seconds lapsed from the date setting
function valuateDate() {
	// set variables
	let date_start = new Date('April 12, 2018 05:40:00');
	let date_now = Date.now();
	let start_value = date_start.valueOf();
	let now_value = date_now.valueOf();

	// valuate date to seconds
	let timeValue = now_value - start_value;
	let strTime = timeValue.toString();
	let seconds = strTime.slice(0,-3);
	seconds = parseInt(seconds);

	// display value of TIME on view
	let seconds_holder = document.getElementById('seconds-count');
	seconds_holder.innerHTML = numberWithCommas(seconds);

	// pass value to next function
	valuateColor(seconds);
}

function valuateColor(seconds) {
	// set variables
	let sec_val = seconds/1.88;
	let red = 0, green = 0, blue = 0;
	let bd = "up";
	let gd = "up";
	let rd = "up";

	for (let i = 0; i < sec_val; i++) {
		if (red === 255 && green === 255 && blue === 255) {
			red = 0;
			green = 0;
			blue = 0;
		}
		countBlue();
	}

	function countBlue() {
		if(blue < 255 && bd === "up") {
			blue += 1;
		}
		if(blue === 255) {
			bd = "down";
			countGreen();
		}
		if(blue > 0 && bd === "down") {
			blue -= 1;
		}
		if(blue === 0) {
			bd = "up";
			countGreen();
		}
	}

	function countGreen() {
		if(green < 255 && gd === "up") {
			green += 1;
		}
		if(green === 255) {
			gd = "down";
			countRed();
		}
		if(green > 0 && gd === "down") {
			green -= 1;
		}
		if(green === 0) {
			gd = "up";
			countRed();
		}
	}

	function countRed() {
		if(red < 255 && rd === "up") {
			red += 1;
		}
		if(red === 255) {
			rd = "down";
		}
		if(red > 0 && rd === "down") {
			red -= 1;
		}
		if(red === 0) {
			rd = "up";
		}
	}

	// display value of ITERRATIONS on view
	let sec_val_parsed = parseInt(sec_val);	
	sec_val_final = combinationValue(sec_val_parsed);

	console.log(sec_val_parsed, 'sec val parsed 16777216')

	let seconds_value_holder = document.getElementById('seconds-value');
	seconds_value_holder.innerHTML = numberWithCommas(sec_val_final);

	// display values of RGB on view
	let color_holder = document.getElementById('color-value');
	color_holder.innerHTML = [red + "-" + rd, green + "-" + gd, blue + "-" + bd];

  let color_red = document.getElementById('color-red');
  if (color_red) {
    color_red.innerHTML = red;
  }
  let color_green = document.getElementById('color-green');
  if (color_green) {
    color_green.innerHTML = green;
  } 
  let color_blue = document.getElementById('color-blue');
  if (color_blue) {
    color_blue.innerHTML = blue;
  }

  let color_reddirection = document.getElementById('color-red-direction');
  if (color_reddirection) {
    color_reddirection.innerHTML = rd;
  }
  let color_greendirection = document.getElementById('color-green-direction');
  if (color_greendirection) {
    color_greendirection.innerHTML = gd;
  } 
  let color_bluedirection = document.getElementById('color-blue-direction');
  if (color_bluedirection) {
    color_bluedirection.innerHTML = bd;
  }

	// pass value to next function
	setColor(red, green, blue);
}

//valuate combination number
function combinationValue(number) {
	let howMany = number/16777216;
	let howManyFloor = Math.floor(howMany);
	let deleteComb = howManyFloor*16777216;
	let finalComb = number - deleteComb;
	return finalComb;
}

function numberWithCommas(number) {
	return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

// render color to div
function setColor(red, green, blue) {
	let bgc_val = [red, green, blue];
	let rv = bgc_val[0];
	let gv = bgc_val[1];
	let bv = bgc_val[2];

	// set value on plane
	let plane = document.getElementById('plane');
	plane.style.backgroundColor = 'rgb(' + rv + ',' + gv + ',' + bv + ')';
}

window.setInterval(function(){ masterHandler() }, 1000);
